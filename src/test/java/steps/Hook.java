package steps;

import base.BaseUtil;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hook extends BaseUtil {

    private BaseUtil base;

    public Hook(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void initializeTest(){
        System.out.println("\nOpening the browser : mock\n");

        //passing a dummy WebDriver instance
        base.stepInfo = "FirefoxDriver";
    }

    @After
    public void tearDownTest(Scenario scenario){
        if (scenario.isFailed()){
            // do something
            System.out.println(scenario.getName());
        }
        System.out.println("\nClosing the browser: mock\n");
    }
}
