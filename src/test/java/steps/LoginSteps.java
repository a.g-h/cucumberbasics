package steps;

import base.BaseUtil;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import transformation.EmailTransformer;
import transformation.SalaryDigitCountTransformer;

import java.util.ArrayList;
import java.util.List;

public class LoginSteps extends BaseUtil {

    private BaseUtil base;

    public LoginSteps(BaseUtil base) {
        this.base = base;
    }

    @Then("^I should see the userform page$")
    public void iShouldSeeTheUserformPage()throws Throwable{
        System.out.println("The driver is: " + base.stepInfo);
        System.out.println("I should see the userformpage");
    }

    @Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() throws Throwable{

    }

    @And("^I click login button$")
    public void iClickLoginButton() throws Throwable {

    }

    /*@And("^I enter the username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void iEnterTheUsernameAsAndPasswordAs(String username, String password) throws Throwable {
        System.out.println("username: " + username + "\npassword: " + password);
    }*/

    @And("^I enter the following for Login$")
    public void iEnterTheFollowingForLogin(DataTable table) throws Throwable {
        List<List<String>> data = table.raw();
        /*System.out.println("The value is : " + data.get(0).get(0));
        System.out.println("The value is : " + data.get(0).get(1));*/

        List<User> users = new ArrayList();

        users = table.asList(User.class);
        users.forEach(user -> System.out.println(user.username + " : " + user.password));
    }

    @And("^I enter ([^\"]*) and ([^\"]*)$")
    public void iEnterUsernameAndPassword(String username, String password) throws Throwable {
        System.out.println("Username: " + username);
        System.out.println("Password: " + password);
    }

    @And("^I enter the users email address as Email:([^\"]*)$")
    public void iEnterTheUsersEmailAddressAsEmailAdmin(@Transform(EmailTransformer.class) String email) throws Throwable {
        System.out.println("The email address is: " + email);
    }

    @And("^I verify the count of my salary digits for byr (\\d+)$")
    public void iVerifyTheCountOfMySalaryDigitsForByr(@Transform(SalaryDigitCountTransformer.class) int salary) throws Throwable {
        System.out.println("My salary digits count is: " + salary);
    }

    public class User
    {
        private String username;
        private String password;

        public User(String username, String password) {
            this.username = username;
            this.password = password;
        }


    }


}
